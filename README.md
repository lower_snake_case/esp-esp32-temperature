# ESP 32 DevKit

## Setup

<https://docs.espressif.com/projects/esp-idf/en/stable/get-started/#>

## TTY
### Command
`dmesg --follow`

### Output
`--snip--`
`[ 1450.076024] usb 3-3: cp210x converter now attached to ttyUSB3`
